import java.time.LocalDateTime;

public class IntervalPolling {
    private LocalDateTime _nextRuntime;

    public IntervalPolling() {
        LocalDateTime now = LocalDateTime.now();

    }

    public void poll() {

    }

    public LocalDateTime getNextRuntime(LocalDateTime now) {
        // Figure out time between now and next Friday-at-1:30-PM-CST

        LocalDateTime nextRuntime = now;

        int minuteOfHour = nextRuntime.getMinute();

        nextRuntime = nextRuntime.plusMinutes(minuteOfHour <= 30 ?
                30 - minuteOfHour :
                90 - minuteOfHour);

        int hourOfDay = nextRuntime.getHour();

        nextRuntime = nextRuntime.plusHours(hourOfDay <= 13 ?
                13 - hourOfDay :
                37 - hourOfDay);

        int dayOfWeek = nextRuntime.getDayOfWeek().getValue(); // Starts with MONDAY = 1, FRIDAY = 5

        nextRuntime = nextRuntime.plusDays(dayOfWeek <= 5 ?
                5 - dayOfWeek :
                12 - dayOfWeek);

        return now.isAfter(nextRuntime) ?
                nextRuntime.plusDays(7) :
                nextRuntime;
    }
}
