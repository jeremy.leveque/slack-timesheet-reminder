import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

class IntervalPollingTests {
    private final LocalDateTime CORRECT_TIME =
            LocalDateTime.of(2021, 7, 23, 13, 30);
    private final LocalDateTime NEXT_CORRECT_TIME = CORRECT_TIME.plusDays(7);

    private IntervalPolling _intervalPolling;

    @BeforeEach
    void setup() {
        _intervalPolling = new IntervalPolling();
    }

    @Test
    void testReturnsNextRuntimeWhenProvidedCorrectTime() {
        LocalDateTime nextRuntime = _intervalPolling.getNextRuntime(CORRECT_TIME);

        assertEquals(CORRECT_TIME, nextRuntime);
    }

    @Test
    void testReturnsNextRuntimeWhenMinutesEarly() {
        LocalDateTime earlyByMinutes = CORRECT_TIME.withMinute(11);

        LocalDateTime nextRuntime = _intervalPolling.getNextRuntime(earlyByMinutes);

        assertEquals(CORRECT_TIME, nextRuntime);
    }

    @Test
    void testReturnsNextRuntimeWhenMinutesLate() {
        LocalDateTime lateByMinutes = CORRECT_TIME.withMinute(41);

        LocalDateTime nextRuntime = _intervalPolling.getNextRuntime(lateByMinutes);

        assertEquals(NEXT_CORRECT_TIME, nextRuntime);
    }

    @Test
    void testReturnsNextRuntimeWhenHoursEarly() {
        LocalDateTime earlyByHours = CORRECT_TIME.withHour(5);

        LocalDateTime nextRuntime = _intervalPolling.getNextRuntime(earlyByHours);

        assertEquals(CORRECT_TIME, nextRuntime);
    }

    @Test
    void testReturnsNextRuntimeWhenHoursLate() {
        LocalDateTime lateByHours = CORRECT_TIME.withHour(15);

        LocalDateTime nextRuntime = _intervalPolling.getNextRuntime(lateByHours);

        assertEquals(NEXT_CORRECT_TIME, nextRuntime);
    }
}
